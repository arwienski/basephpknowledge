<?php

function convertFullName($string)
{
    $words = explode (" ", $string);
    $namesLetter = mb_substr($words[1], 0, 1);
    $patronymicLetter = mb_substr($words[2], 0, 1);
    $result = $words[0]." ".$namesLetter.". ".$patronymicLetter.".";
    return $result; // Результат: Фамилия И.О.
}

echo convertFullName('Иванов Иван Иванович');