<?php

$array = [
    [
        'sort' => '20',
        'name' => 'Mike'
    ],
    [
        'sort' => '10',
        'name' => 'Adam'
    ],
    [
        'sort' => '40',
        'name' => 'Stive'
    ],
    [
        'sort' => '300',
        'name' => 'Jane'
    ],
];

function compareDesc($a, $b){
	return ($a['sort'] < $b['sort']);
}

uasort($array, 'compareDesc');
print_r($array);